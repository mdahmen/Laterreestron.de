function part1() {
    metroBeatsSeconds(120);
    partFract(2, 4);
    note("do4", 1);
    note("re4", 1);
    note("mi4", 1);
    note("fa4", 1);
    note("sol4", 1);
    note("la4", 1);
    note("si4", 1);
    note("do5", 1);
}
function partFractDenominateur() {
    return partFractDenominateur;
}
function partFractNumerateur() {
    return partFractNumerateur;
}
function partFract(num, denom) {
    partFractNumerateurVar = num;
    partFractDenominateurVar = denom;
}
function part1_stop() {
    T.stop();
}