/***
 * http://mohayonao.github.io/timbre.js/PragmaSynth.html
 * @type {number}
 */

var tempo = 120;
function metroBeatsSeconds(seconds) {
    tempo = seconds;
    timbre.bpm = seconds;
}
var synth = T("OscGen", {
    wave: "saw",
    mul: 0.25
}).play();

var keydict = T("ndict.key");
var midicps = T("midicps");
T("keyboard").on("keydown", function (e) {
    var midi = keydict.at(e.keyCode);
    if (midi) {
        var freq = midicps.at(midi);
        synth.noteOnWithFreq(freq, 100);
    }
}).on("keyup", function (e) {
    var midi = keydict.at(e.keyCode);
    if (midi) {
        synth.noteOff(midi, 100);
    }
}).start();
/**
 * Created by manuel on 13-03-16.
 */
function freqSon(ecartTon) {
    var BaseLa4 = 440;
    var factTon = Math.pow(2, 1 / 6);
    return BaseLa4 * Math.pow(factTon, ecartTon);


}
function note(param, length) {
    var fre = freqSon(noteTrans(param, length));
    var dureeMs = (length / timbre.bpm / 60 * 1000);
    console.log("Note" + param, " \nDurée (en temps de mesure): " + (length * 1000 / partFractDenominateur()) + "ms (");
    //var osc = T("sin", {freq: fre, mul: 0.4, add: 1}).play();
    synth.noteOnWithFreq(fre, dureeMs);
    //synth.noteOff(midi, dureeMs);
    /*
    T("timeout", {timeout: "" + (length * 1000 / timbre.bpm) + "ms"}).on("ended", function () {
        osc.stop();
    });
     */
}

/***
 * ex; do4 , 1
 * ex: so4 , 1
 */
function noteTrans(note, length) {
    switch (note.substring(0, 2)) {
        case "do":
            param = 0;
            break;
        case "re":
            param = 1;
            break;
        case "mi":
            param = 2;
            break;
        case "fa":
            param = 2.5;
            break;
        case "so":
            param = 3.5;
            break;
        case "la":
            param = 4.5;
            break;
        case "si":
            param = 5.5;
            break;
        default:
            break;
    }
    octave = note.substring(2, 3);

    if (note.substring(-1) == "#") {
        param += 0.5;
    }
    if (note.substring(-1) == "b") {
        param -= 0.5;
    }


    var ecart = param + 6 * octave - 4.5 + 6 * 4; //ParRapprtLa440 en tons + -> aigu - -> grave

    return ecart;
}

function silence(length) {
}
/*
 readTextFileDegree1(docString);
 {
 for (line in docString) {
 readBPM();
 readFract();
 loopReadNote();

 }
 }

 */
var synthClavier = true;
function synthClavierOnOff() {
    synthClavier != synthClavier;

    if (synthClavier) {
        $("#keyboard").type = "text";
    }
    else {
        $("#keyboard").type = "button";
    }
}