<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Partitions</title>
    <script src="js/libs/jquery/jquery-1.12.1.min.js"></script>
    <script src="http://mohayonao.github.io/timbre.js/timbre.js"></script>
    <script src="http://mohayonao.github.io/timbre.js/src/extras/keyboard.js"></script>
    <script src="js/lecture.js"></script>
    <script src="data/part1.js"></script>
    <script>
        function onclickNoteButton() {
            //var noteText = document.getElementById("NoteText");
            //var noteLongueur = document.getElementsByClassName("NoteLongueur");
            var isSound = document.forms.choixNoteForm.noteType.value == "1";
            noteTrans(document.forms.choixNoteForm.noteText.value, document.forms.choixNoteForm.NoteLongueur.value);
        }
    </script>
</head>
<body>
    <h1>Partitions</h1>
<form name="choixNoteForm">
    <table style="width:50%; border: 2px inset #666699;">
        <tr>
            <td>
                <fieldset>
                    <input type="button" id="SynthClavier" name="noteText" value="Activer/désactiver les sons clavier"
                           onclick="synthClavierOnOff();"/>
                </fieldset>
                <fieldset>
                    <input type="text" id="NoteText" name="noteText" value=""/>
                </fieldset>
                <fieldset>
                    <input type="radio" name="NoteLongueur" value="1">Ronde</input>
                    <input type="radio" name="NoteLongueur" value="1">Blanche</input>
                    <input type="radio" name="NoteLongueur" value="1" checked="checked">Noire</input>
                    <input type="radio" name="NoteLongueur" value="0.5">Croche</input>
                    <input type="radio" name="NoteLongueur" value="1">Double croches</input>
                </fieldset>
                <fieldset>
                    <input type="radio" class="NoteType" name="noteType" value="1" checked="checked">Note</input>
                    <input type="radio" class="NoteType" name="noteType" value="0">Silence</input>
                </fieldset>
                <fieldset>
                    <button id="btn_play" class="audioControls" onclick="part1();" value="part1 play">Start part2
                    </button>
                    <button id="btn_stop" class="audioControls" onclick="part1_stop();" value="part1 stop">Stop part1
                    </button>
                </fieldset>
                <fieldset>
                    <input type="button" onclick="onclickNoteButton()" value="Jouer"/>
                    <input type="button" onclick="onclickNoteButton()" value="Jouer et ajouter"/>
                </fieldset>
                </fieldset>
            </td>
            <td>
                <script>T("keyboard");</script>
            </td>
        </tr>
    </table>
</form>
<div>KeyCode: <span id="key"></span>, Freq: <span id="freq"></span></div>
    <input id="keyboard" placeholder="Appuyer sur les touches du clavier (lettre et rangée de chiffres)">
</body>
</html>